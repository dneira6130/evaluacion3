<%-- 
    Document   : index
    Created on : 20-abr-2020, 20:04:00
    Author     : junior
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="root.model.entities.Usuario"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%
  
  List<Usuario> usuarios=new ArrayList<Usuario>();
  
  if(request.getAttribute("usuarios")!=null){
      usuarios=(List<Usuario>)request.getAttribute("usuarios");
  
  }
   Iterator<Usuario> itUsuarios=usuarios.iterator();
%> 
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet"
       
          href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">     
        <title>Lista de Algo</title>
    </head>
    <body class="text-center" >
     <form name="form" action="controllerListado" method="POST">          
    <div class="cover-container d-flex h-100 p-3 mx-auto flex-column">
      <table border="1">
                <thead>
                <th>username</th>
                <th>nombre </th>
                <th>Apellido </th>
    <th> </th>
                </thead>
                <tbody>
               <%while(itUsuarios.hasNext()){
                  Usuario usu=itUsuarios.next();%>
                <tr>
                   <td><%= usu.getUsername() %></td>
                   <td><%= usu.getNombre() %></td>
                   <td><%= usu.getApellido() %></td>
        <td> <input type="radio" name="seleccion" value="<%= usu.getUsername() %>"> </td>
           
                </tr>
               <%}%>                
                </tbody>           
            </table>
          <button type="submit" name="accion" value="editar" class="btn btn-lg btn-secondary" style="margin-top: 10px;margin-bottom: 10px;">Editar</button>
           <button type="submit" name="accion" value="eliminar" class="btn btn-lg btn-secondary" style="margin-top: 10px;margin-bottom: 10px;">Eliminar</button>
         <button type="submit"  name="accion"  value="crear" class="btn btn-lg btn-secondary" style="margin-top: 10px;margin-bottom: 10px;">Crear</button>
          <button type="submit"  name="accion"  value="listar" class="btn btn-lg btn-secondary" style="margin-top: 10px;margin-bottom: 10px;">Listar</button>
 
     </form>
    </body>
</html>