<%-- 
    Document   : index
    Created on : 20-abr-2020, 20:04:00
    Author     : junior
--%>


<%@page import="root.model.entities.Usuario"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%
  Usuario usuario=(Usuario)request.getAttribute("usuario");
 
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet"
         href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
        <title>Test Lista</title>
    </head>
    <body class="text-center" >
    <div class="cover-container d-flex h-100 p-3 mx-auto flex-column">
      <header class="masthead mb-auto">
        <div class="inner">
    
        </div>
      </header>
    </div>
      <main role="main" class="inner cover">
        <h1 class="cover-heading">Editar</h1>
        <p class="">
            
             <form  name="form" action="controllerUsuario" method="POST">
                    <div class="form-group">
                        <label for="rut">Username</label>
                        <input  name="rut" value="<%= usuario.getRut() %>" class="form-control" required id="rut" aria-describedby="rutHelp">
                           </div>
                    <br>
                    <div class="form-group">
                        <label for="nombre">Nombre</label>
                        <input  step="any" name="nombre" value="<%= usuario.getNombre()%>"  class="form-control" required id="nombre" aria-describedby="nombreHelp">
                     </div>       
                    <br>
                    <div class="form-group">
                        <label for="apellido1">Apellido Paterno</label>
                         <input  name="apellido1" class="form-control"  value="<%= usuario.getApellido1()%>"  required id="apellido1" aria-describedby="apellido1Help">
                 
                      </div>        
                    <br>
                    <br>
                    <div class="form-group">
                        <label for="apellido2">Apellido Materno</label>
                         <input  name="apellido2" class="form-control"  value="<%= usuario.getApellido2()%>"  required id="apellido2" aria-describedby="apellido2Help">
                 
                      </div>        
                    <br>
                    <br>
                    <div class="form-group">
                        <label for="correo">Correo</label>
                         <input  name="correo" class="form-control"  value="<%= usuario.getCorreo()%>"  required id="correo" aria-describedby="correoHelp">
                 
                      </div>        
                    <br>
                    <br>
                    <div class="form-group">
                        <label for="fono">Telefono</label>
                         <input  name="fono" class="form-control"  value="<%= usuario.getFono()%>"  required id="fono" aria-describedby="fonoHelp">
                 
                      </div>        
                    <br>
                    <button type="submit" name="accion" value="grabarEditar" class="btn btn-success">Grabar</button>
                     <button type="submit" name="accion" value="salirEditar"  class="btn btn-success">Salir</button>
                </form>
  
      </main>

  
      
       
    </body>
</html>